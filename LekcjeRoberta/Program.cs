﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LekcjeRoberta
{
    class Program
    {
        static void Main(string[] args)
        {
            var message = new Wiadomosc();
            message.WypiszZObiektu();

            Wiadomosc.WypiszZKlasy();

        }
    }

    class Wiadomosc
    {
        string tekstWObiekcie;
        static string tekstWKlasie;

        public Wiadomosc()
        {
            var aktualnyCzas = DateTime.Now;

            if(aktualnyCzas.Hour < 6 || aktualnyCzas.Hour > 18)
                tekstWObiekcie = "Jest dzien";
            else
                tekstWObiekcie = "Jest noc";
        }

        public void WypiszZObiektu()
        {
            Console.WriteLine(tekstWObiekcie);
        }

        public static void WypiszZKlasy()
        {
            var aktualnyCzas = DateTime.Now;

            if (aktualnyCzas.Hour < 6 || aktualnyCzas.Hour > 18)
                tekstWKlasie = "Jest dzien";
            else
                tekstWKlasie = "Jest noc";

            Console.WriteLine(tekstWKlasie);
        }
    }
}
